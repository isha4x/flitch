<p align="center">
  <img src="/assets/logo-xl.png" width="150" />
</p>
<p align="center">⭐⭐⭐⭐⭐</p>
<h1 align="center">Anime Dart</h1>
<h4 align="center"><a href="https://github.com/alexrintt/anime-dart/releases/"><code>Download Apk</code></a></h4>
<p align="center">Um aplicativo para assistir e fazer download de episódios de Animes, disponível para Android, totalmente open-source.</p>

<p align="center">
  <img  src="https://img.shields.io/badge/development-mobile-purple" alt="Application Category" />
  <img  src="https://img.shields.io/badge/language-dart-blue" alt="Repo Main Language" />
  <img  src="https://img.shields.io/badge/technology-flutter-blue" alt="Module Bundler" />
  <img  src="https://img.shields.io/badge/type-project-success" alt="Repo Type" />
  <img  src="https://img.shields.io/badge/name-anime_dart-green" alt="Game Ref" />
</p>

<p align="center">
  <a href="https://www.linkedin.com/in/alexrintt" target="_blank">
    <img src="https://img.shields.io/twitter/url?label=Connect%20%40alexrintt&logo=linkedin&url=https%3A%2F%2Fwww.twitter.com%2Falexrintt%2F" alt="Follow" />
  </a>
  <a href="https://www.linkedin.com/in/emmanuel-messias-535621127/" target="_blank">
    <img src="https://img.shields.io/twitter/url?label=Connect%20%40Emmanuel&logo=linkedin&url=https%3A%2F%2Fwww.twitter.com%2Falexrintt%2F" alt="Follow" />
  </a>
</p>

<br>
<h3 align="center">💜</h3>
<p align="center"><b>Quero agradecer especialmente ao <a href="https://github.com/mannoeu">@Emmanuel</a> por ter criado o ícone do aplicativo! =D</b></p>
<br>

<br>
<br>

> _**Note: this application is in PT-BR, but if you know another API in another idiom, feel free to contact me or open an issue to include your language in the App**_

## Table of contents

- [Printscreens - Dark Mode](#printscreens---dark-mode)
- [Printscreens - Light Mode](#printscreens---light-mode)
- [Como clonar este projeto](#como-clonar-este-projeto)

## Support

If you have ideas to share, bugs to report or need support, you can either open an issue or join our Discord server

<a href="https://discord.gg/86GDERXZNS">
  <kbd><img src="https://discordapp.com/api/guilds/771498135188799500/widget.png?style=banner2" alt="Discord Banner"/></kbd>
</a>

## Printscreens - Dark Mode

<table>
  <tr>
    <td>
      <img src="/assets/lan.jpeg" width="250">
    </td>
    <td>
      <img src="/assets/random.jpeg" width="250">
    </td>
    <td>
      <img src="/assets/cat.jpeg" width="250">
    </td>
  </tr>
  <tr>
    <td>
      <img src="/assets/fav.jpeg" width="250">
    </td>
    <td>
      <img src="/assets/ep-de.jpeg" width="250">
    </td>
    <td>
      <img src="/assets/cat-bottom.jpeg" width="250">
    </td>
  </tr>
  <tr>
    <td>
      <img src="/assets/ep-de-no-view.jpeg" width="250">
    </td>
    <td>
      <img src="/assets/ep-list.jpeg" width="250">
    </td>
    <td>
      <img src="/assets/ep-list-no-view.jpeg" width="250">
    </td>
  </tr>
  <tr>
    <td>
      <img src="/assets/marcar-como.jpeg" width="250">
    </td>
    <td>
      <img src="/assets/rec.jpeg" width="250">
    </td>
    <td>
      <img src="/assets/search.jpeg" width="250">
    </td>
  </tr>
</table>

## Printscreens - Light Mode

<table>
  <tr>
    <td>
      <img src="/assets/ep-list-l.jpeg" width="250">
    </td>
    <td>
      <img src="/assets/rec-l.jpeg" width="250">
    </td>
    <td>
      <img src="/assets/marcar-como-l.jpeg" width="250">
    </td>
  </tr>
  <tr>
    <td>
      <img src="/assets/cat-l.jpeg" width="250">
    </td>
    <td>
      <img src="/assets/ep-de-l.jpeg" width="250">
    </td>
    <td>
      <img src="/assets/cat-bottom-l.jpeg" width="250">
    </td>
  </tr>
  <tr>
    <td>
      <img src="/assets/random-l.jpeg" width="250">
    </td>
    <td>
      <img src="/assets/fav-l.jpeg" width="250">
    </td>
  </tr>
  <tr>
    <td>
      <img src="/assets/lan-l.jpeg" width="250">
    </td>
    <td>
      <img src="/assets/search-l.jpeg" width="250">
    </td>
  </tr>
</table>

<br>
<br>
<br>

## Arquivos de instalação para Android

Nesta release estou optando por oferecer o fat apk (arquivo com os binários das 3 arquiteturas do Android), portanto, basta scanear o QRCode abaixo e instalar, sem se preocupar com a arquitetura.

([Flutter Docs](https://flutter.dev/docs/deployment/android#what-is-a-fat-apk)).

<br>
<br>
<br>

## Como clonar este projeto

### 1. Clone o repositório

```
git clone https://github.com/alexrintt/anime-dart.git
```

### 2. Mude a pasta

```
cd anime-dart
```

### 3. Instale as dependências

```
flutter pub get
```

### 4. Debug Mode

Press F5 if Vscode or run in Command-Line:

```
flutter run
```

### 5. Gere a build

```
flutter build apk --split-per-abi
```

<br>
<br>
<br>

<samp>

<h2 align="center">
  Open Source
</h2>
<p align="center">
  <sub>Copyright © 2020-present, Alex Rintt.</sub>
</p>
<p align="center">Flitch <a href="https://github.com/alexrintt/anime-dart/blob/master/LICENSE.md">is MIT licensed 💖</a></p>
<p align="center">
  <img src="./assets/logosmall.png" height="100" />
</p>

</samp>
